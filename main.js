var five = require('johnny-five'), board = new five.Board();
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var anim;
var animRunning;
//leds

app.get("/",function(req,res){
	res.sendFile(__dirname+"/control/index.html");
});



http.listen(3000,function(){
	console.log("server started");
});

board.on("ready",function(){
	var leds = [new five.Led(9),new five.Led(11), new five.Led(12)];
	startupAnim();
	
	
	this.repl.inject({
		stopAll: stopAll,
		anim1:anim1,
		anim2:anim2,
		anim3:anim3
	});
	
	io.on('connection',function(socket){
		console.log("client connected");
		socket.on('disconnect', function(){
			console.log('client disconnected');
		});
		socket.on("playAnim",function(anim){
			if(anim=="Anim 1") { stopAll();anim1(); }
			else if(anim=="Anim 2") { stopAll();anim2(); }
			else if(anim=="Anim 3") { stopAll();anim3(); }
			
			console.log("play animation "+anim);
		});
		socket.on("stopAnim",function(){
			stopAll();
			console.log("stop animation");
		});
		socket.on("flashOn",function(){
			if(animRunning){
				stopAll();
			}
			leds[2].on();
			console.log("flash on");
		});
		socket.on("flashOff",function(){
			leds[2].off();
			console.log("flash off");
		});
		socket.on("redLight",function(val){
			if(animRunning){
				stopAll();
			}
			leds[1].on();
			leds[1].brightness(val);
			console.log("Red light brightness:"+val);
		});
		socket.on("whiteLight",function(val){
			if(animRunning){
				stopAll();
			}
			leds[0].on();
			leds[0].brightness(val);
			console.log("White Light brightness:"+val);
		});
		
	});
	
	
	function startupAnim(){
		leds[2].strobe(500);
		setTimeout(function(){
			leds[2].stop().off();
			console.log("Ready");
		},2000);
	}
	
	function anim1(){
		stopAll();
		leds[2].strobe(Math.floor(Math.random()*(1000-100)+100));
		leds[1].pulse(200);
		leds[0].pulse(1200);
		animRunning = true;
	}
	
	function anim2(){
		stopAll();
		leds[0].strobe(200);
		leds[1].strobe(400);
		leds[2].strobe(600);
		animRunning = true;
	}
	
	function anim3(){
		stopAll();
		animRunning = true;
		function loop(){
			leds[0].on();
			setTimeout(function(){
				leds[0].off();
				leds[1].on();
			},800);
			setTimeout(function(){
				leds[1].off();
				leds[2].on();
			},1600);
			setTimeout(function(){
				leds[2].off();
			},2400);
		}
		anim = setInterval(function(){
			loop();
		},2400);
		
		
	}
	// function alert(){
		// stopAll();
		// leds[2].blink(200);
		// setTimeout(function(){
			// leds[2].stop().off();
		// },1600);
	// }
	function stopAll(){
		leds[0].stop().off();
		leds[1].stop().off();
		leds[2].stop().off();	
		clearInterval(anim);
		animRunning=false;
	}
});
